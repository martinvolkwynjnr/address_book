﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Address_Book._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="">
        <h1>View Contact Details</h1>
        <asp:Button Text="ADD" runat="server" OnClick="addAddress"/>
        <asp:Button Text="EDIT" runat="server" OnClick="editAddress"/>
        <asp:Button Text="DELETE" runat="server" OnClick="deleteAddress"/>
        <asp:TextBox ID="searchTxt" runat="server" OnTextChanged="Search_TextChanged"></asp:TextBox>

        <asp:PlaceHolder ID="tablePlaceholder" runat="server"></asp:PlaceHolder>
        <%--<asp:SqlDataSource runat="server" ConnectionString="Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=addressDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"></asp:SqlDataSource>--%>
        <%--<asp:Table></asp:Table>--%>
    </div>

</asp:Content>
