﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Address_Book.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
       <asp:Label ID="firstNameLbl" Text="First Name" runat ="server"></asp:Label>
       <asp:TextBox ID="firstNameInput" OnTextChanged="firstNameInput_TextChanged" runat="server"></asp:TextBox>
   </div>
    <div>
       <asp:Label ID="LastNameLbl" Text="Last Name" runat ="server"></asp:Label>
       <asp:TextBox ID="LastNameInput" OnTextChanged="LastNameInput_TextChanged" runat="server"></asp:TextBox>
   </div>
    <div>
       <asp:Label ID="ContactLbl" Text="Contact Number" runat ="server"></asp:Label>
       <asp:TextBox ID="ContactInput" OnTextChanged="ContactInput_TextChanged" runat="server"></asp:TextBox>
   </div>
    <div>
       <asp:Label ID="EmailLbl" Text="Email" runat ="server"></asp:Label>
       <asp:TextBox ID="EmailInput" OnTextChanged="EmailInput_TextChanged" runat="server"></asp:TextBox>
   </div>
    <div>
        <asp:Button text="SUBMIT" runat="server" ID="submitBTN" OnClick="submitBTN_Click" />
    </div>
</asp:Content>
