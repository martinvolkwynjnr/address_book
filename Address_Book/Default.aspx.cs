﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Address_Book
{
    public partial class _Default : Page
    {
        StringBuilder table = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select C.[contactID], C.[First Name],C.[Last Name],COM.[number], COM.[email] from dbo.ContactID as C join dbo.Communication  as COM on C.contactID = COM.contactID";
                cmd.Connection = con;
                SqlDataReader rd = cmd.ExecuteReader();
                table.Append("<table border='1'>");
                table.Append("<tr><th>Contact_ID</th><th>First Name</th><th>Last Name</th><th>Number</th><th>Email</th>");
                table.Append("</tr>");

                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        table.Append("<tr>");
                        table.Append("<td>" + rd[0] + "</td>");
                        table.Append("<td>" + rd[1] + "</td>");
                        table.Append("<td>" + rd[2] + "</td>");
                        table.Append("<td>" + rd[3] + "</td>");
                        table.Append("<td>" + rd[4] + "</td>");
                        table.Append("</tr>");
                    }

                }
                table.Append("</table>");
                tablePlaceholder.Controls.Add(new Literal { Text = table.ToString() });
                rd.Close();
                
            }

        }

        protected void addAddress(object sender, EventArgs e)
        {
            Response.Redirect("Add.aspx");
        }

        protected void editAddress(object sender, EventArgs e)
        {
            string script = "alert(\"editAddress!\");";
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "ServerControlScript", script, true);
        }

        protected void deleteAddress(object sender, EventArgs e)
        {
            string script = "alert(\"deleteAddress!\");";
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "ServerControlScript", script, true);
        }

        protected void Search_TextChanged(object sender, EventArgs e)
        {
            string text = searchTxt.Text;
            string script = "alert(\"" + text + "!\");";
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "ServerControlScript", script, true);
        }
    }
}